# Tilemaker

v: Alpha!

- Based on this: https://github.com/commenthol/gdal2tiles-leaflet
- This script uses 'gdal2tiles.py' rather than the faster 'gdal2tiles-multiprocess.py' because for some reason the latter drops tiles on output. I haven't had time to investigate why.

## Generate tiles

0. Make sure your source files are divisible by 256 in both directions (optional but yields better result I think, need to verify)

1. Put source file in src/FOLDER/FILE. You can have multiple pages, so
   /src/document/page1.png, /src/document/page2.png, etc.

1. `generateTilesFor FOLDER` Your output will be in /dst

## To preview:

1. Edit "tileserver" in www/index.js
2. ./preview
3. Go to http://localhost:9000

## Workflow from PDF

- `shell convert -density 300 -quality 95 mag_ok_sbs.pdf zine.png`
- cleanup with PSD action
