#!/bin/bash
FILENAME=${1##*/}
echo output to $2/$FILENAME

mkdir -p $2/$FILENAME
./scripts/vendor/gdal2tiles-retina.py -e -l -p raster -w none -z 0-4 $1 $2/$FILENAME
./scripts/vendor/gdal2tiles.py -e -l -p raster -w none -z 0-4 $1 $2/$FILENAME
